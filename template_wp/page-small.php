<?php /* Template Name: Small container */ ?>

<?php get_header(); ?>

<section class="main">
    <div class="container">
        <div class="container-small">
            <?php
            if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();
                ?>

                    <?php the_content(); ?>

                <?php
                endwhile;

                the_posts_navigation();

            else :

                ?><h1>Niks gevonden</h1><?php

            endif; ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>
