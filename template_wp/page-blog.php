<?php get_header(); ?>

<section class="blog">
    <div class="container">
        <div class="container-small">
            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $query_args = array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'paged' => $paged
            );
            $the_query = new WP_Query( $query_args );
            ?>

            <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>
                <article>
                    <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo the_title(); ?></a></h1>
                    <div class="content">
                        <?php the_excerpt(); ?>
                    </div>
                </article>
            <?php endwhile; ?>

            <?php if ($the_query->max_num_pages > 1) { ?>
                <nav class="pagnation">
                    <div class="prev-posts-link">
                        <?php echo get_next_posts_link( 'Oudere berichten', $the_query->max_num_pages ); // display older posts link ?>
                    </div>
                    <div class="next-posts-link">
                        <?php echo get_previous_posts_link( 'Recentere berichten' ); // display newer posts link ?>
                    </div>
                </nav>
                <?php } ?>

            <?php else: ?>
                <article>
                    <h1>Sorry...</h1>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                </article>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
