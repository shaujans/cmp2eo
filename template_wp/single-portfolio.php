<?php get_header(); ?>
<section class="portfolio">
    <article class="portfolio__item" data-color="<?php the_field("kleur"); ?>">
        <div class="container">
            <div class="item__website--desktop">
                <figure>
                    <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                </figure>
            </div>
            <div class="item__website--mobile">
                <figure>
                    <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                </figure>
            </div>

            <div class="item__content">
                <h2><?php the_field("titel"); ?></h2>

                <?php the_field("beschrijving"); ?>

                <div class="item__action">
                    <?php the_field("actie"); ?>
                </div>
            </div>
        </div>
    </article>
</section>

<?php if(is_user_logged_in()) { ?>
<section class="forum">
    <div class="container">
        <div class="container-small">
            <?php the_field("forum_id"); ?>
        </div>
    </div>
</section>
<?php } ?>

<section class="testimonial">
    <div class="container">
        <div class="container-small">
            <p class="testimonial__content">
                <?php the_field("testimonial_content", 58); ?>
            </p>
            <p class="testimonial__author">
                <?php the_field("testimonial_auteur", 58); ?> <?php if( get_field("testimonial_organisatie", 58) ) { ?><span class="testimonial__seperator"></span><?php } ?> <?php the_field("testimonial_organisatie", 58); ?>
            </p>
        </div>
    </div>
</section>

<?php $hide_resources = true; ?>

<?php get_footer(); ?>
