<?php get_header(); ?>
<section class="portfolio">
    <?php
    $args = array( 'post_type' => 'portfolio', 'posts_per_page' => 10, 'category_name' => 'afgewerkt' );
    $loop = new WP_Query( $args );
    $counter = 0;

    while ( $loop->have_posts() ) : $loop->the_post();
    ?>
    <article class="portfolio__item" data-color="<?php the_field("kleur"); ?>">
        <div class="container">
            <?php if( $counter % 2 == 0 ) { ?>
            <div class="item__website--desktop">
                <figure>
                    <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                </figure>
            </div>
            <?php } else { ?>
                <div class="item__website--mobile">
                    <figure>
                        <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                    </figure>
                </div>
            <?php } ?>

            <div class="item__content">
                <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_field("titel"); ?></a></h2>

                <?php the_field("beschrijving"); ?>

                <div class="item__action">
                    <a class="button" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">Bekijk projectpagina</a>
                </div>
            </div>

            <?php if( $counter % 2 != 0 ) { ?>
                <div class="item__website--desktop hide__mobile">
                    <figure>
                        <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                    </figure>
                </div>
            <?php } ?>
        </div>
    </article>
    <?php
    $counter++;
    endwhile;
    wp_reset_query();
    ?>
</section>

<section class="testimonial">
    <div class="container">
        <div class="container-small">
            <p class="testimonial__content">
                <?php the_field("testimonial_content", 58); ?>
            </p>
            <p class="testimonial__author">
                <?php the_field("testimonial_auteur", 58); ?> <?php if( get_field("testimonial_organisatie", 58) ) { ?><span class="testimonial__seperator"></span><?php } ?> <?php the_field("testimonial_organisatie", 58); ?>
            </p>
        </div>
    </div>
</section>

<?php get_footer(); ?>
