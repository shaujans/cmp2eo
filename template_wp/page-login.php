<?php
/**
 * Template Name: Login
 */

get_header();

global $user_login;
?>

<section class="login">
    <div class="container">
        <div class="container-small">
            <?php if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ): ?>
            <div class="error">
	            <p>Sorry, er is iets fout gelopen. Probeer het opnieuw.</p>
            </div>
            <?php endif; ?>

            <?php if ( is_user_logged_in() ): ?>
            <div class="logout">
                <h1>Dag <?php echo $user_login; ?></h1>
                <p>
                    Je bent reeds ingelogd. <a id="wp-submit" href="<?php echo wp_logout_url(); ?>" title="Logout">Wens je uit te loggen?</a>
                </p>
            </div>
            <?php else:

                // Login form arguments.
                $args = array(
                    'echo'           => true,
                    'redirect'       => home_url( '/lopende-projecten/' ),
                    'form_id'        => 'loginform',
                    'label_username' => __( 'Gebruikersnaam' ),
                    'label_password' => __( 'Wachtwoord' ),
                    'label_remember' => __( 'Onthoud mij en blijf ingelogd' ),
                    'label_log_in'   => __( 'Inloggen' ),
                    'id_username'    => 'user_login',
                    'id_password'    => 'user_pass',
                    'id_remember'    => 'rememberme',
                    'id_submit'      => 'wp-submit',
                    'remember'       => true,
                    'value_username' => NULL,
                    'value_remember' => true
                );

                // Calling the login form.
                wp_login_form( $args );

            endif; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
