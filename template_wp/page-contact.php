<?php get_header(); ?>

<section class="contactinfo">
    <div class="container">
        <div class="container-small">
            <?php
            if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();
                    ?>

                    <?php the_content(); ?>

                    <?php
                endwhile;

                the_posts_navigation();

            else :

                ?><h1>Niks gevonden</h1><?php

            endif; ?>
        </div>
    </div>
</section>

<section class="contactform">
    <div class="container">
        <div class="container-small">
            <?php the_field("contactform_shortcode"); ?>
        </div>
    </div>
</section>

<section class="contactmaps">
    <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:EiBIb29ncG9vcnQgMTUsIDkwMDAgR2VudCwgQmVsZ2nDqw&key=AIzaSyD-qRwqDymbj26IZV9PWfkxEyz-VMxJ5VM" allowfullscreen></iframe>
</section>

<?php get_footer(); ?>
