<?php
/**
 * Created by PhpStorm.
 * User: shaunjanssens
 * Date: 10/04/16
 * Time: 17:24
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.jpg" />
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon@3x.jpg">


    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
    <div class="container">
        <div class="header__menu--mobile">
            <div class="menu__mobiletoggle">Toon menu</div>
            <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
        </div>
        <div class="header__logo">
            <h1>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Terug naar de voorpagina">
                    <picture>
                        <source media="(min-width: 600px)" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-mobile.svg" alt="Webdrop, web design zonder zever.">
                    </picture>
                </a>
            </h1>
        </div>
        <div class="header__menu--desktop">
            <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
        </div>
        <div class="header__title">
            <?php if(is_page('portfolio')) {
                echo "Portfolio";
            } else {
                the_title();
            } ?>
        </div>
    </div>
</header>
