/**
 * Created by shaunjanssens on 10/04/16.
 */
(function( $ ) {
    "use strict";

    $(function() {

        /**
         * Helpers
         * @type {{isMobile: Function}}
         */
        var helpers = {
            /**
             * Check if device is a mobile device
             * @returns {boolean}
             */
            isMobile : function() {
                return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
            }
        };

        /**
         * Parallax
         * @type {{container_header: (*|HTMLElement), container_portfolio_items: (*|HTMLElement), header: Function, portfolio: Function}}
         */
        var parallax = {
            container_header : $("body > header h1"),
            container_portfolio_items : $(".portfolio .portfolio__item"),

            /**
             * Header parallax
             */
            header : function() {
                var item = parallax.container_header;
                var scrolled = $(window).scrollTop();
                var margin = (scrolled * 0.4) * -1;

                $(item).css("margin-top", margin + "px");
            },

            /**
             * Portfolio item parallax
             */
            portfolio : function() {
                var item = parallax.container_portfolio_items;
                var scrolled = $(window).scrollTop();

                $(item).each(function() {
                    var offset = $(this).offset();
                    var margin = (offset.top - scrolled - 200) * 0.2;
                    var image = $(this).find("img");

                    if (margin >= 0) margin = 0;
                    if (margin <= -200) margin = -200;

                    $(image).css("margin-top", margin + "px");
                });
            }
        };

        /**
         * Portfolio
         * @type {{container_portfolio_item: (*|HTMLElement), container_button: string, color: Function}}
         */
        var portfolio = {
            container_portfolio_item : $(".portfolio__item"),
            container_button : ".item__action .button",

            /**
             * Set color with data-color
             */
            color : function() {
                $(portfolio.container_portfolio_item).each(function() {
                    var color = $(this).attr("data-color");
                    $(this).find("h2").css("color", color);
                    $(this).find(portfolio.container_button).css("background-color", color);
                });
            }
        };

        // Set colors
        portfolio.color();

        // Parallax effect on scroll
        $(window).scroll(function () {
            parallax.portfolio();
        });

        // Show contact
        $("#show-contact").on("click", function(e) {
            e.preventDefault();
            $(".contactform .hide").show();
        });

        // Show menu
        $(".menu__mobiletoggle").on("click", function(e) {
            e.preventDefault();
            $("header .header__menu--mobile .menu").toggle();
            $(".menu__mobiletoggle").hide();
        });



    });

}(jQuery));
