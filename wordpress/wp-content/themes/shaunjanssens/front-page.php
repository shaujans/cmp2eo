<?php
/**
 * Created by PhpStorm.
 * User: shaunjanssens
 * Date: 10/04/16
 * Time: 17:25
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
    <div class="container">
        <div class="header__menu--mobile">
            <div class="menu__mobiletoggle">Toon menu</div>
            <?php wp_nav_menu( array( 'theme_location' => 'home-menu' ) ); ?>
        </div>
        <div class="header__menu--desktop">
            <?php wp_nav_menu( array( 'theme_location' => 'home-menu' ) ); ?>
        </div>
        <div class="header__logo">
            <h1>
                <picture>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="Shaun, WordPress webdesign">
                </picture>
            </h1>
        </div>
        <div class="header__action">
            <?php the_field('cta_header'); ?>
        </div>
    </div>
</header>

<section class="about">
    <div class="container">
        <div class="container-small">
            <h1><?php the_field("intro_titel"); ?></h1>

            <?php the_field("intro_content"); ?>
        </div>
    </div>
</section>

<section class="portfolio">
    <?php
    $args = array( 'post_type' => 'portfolio', 'posts_per_page' => 5 );
    $loop = new WP_Query( $args );
    $counter = 0;

    while ( $loop->have_posts() ) : $loop->the_post();
    ?>
    <article class="portfolio__item" data-color="<?php the_field("kleur"); ?>">
        <div class="container">
            <?php if( $counter % 2 == 0 ) { ?>
            <div class="item__website--desktop">
                <figure>
                    <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                </figure>
            </div>
            <?php } else { ?>
                <div class="item__website--mobile">
                    <figure>
                        <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                    </figure>
                </div>
            <?php } ?>

            <div class="item__content">
                <h2><?php the_field("titel"); ?></h2>

                <?php the_field("beschrijving"); ?>

                <div class="item__action">
                    <?php the_field("actie"); ?>
                </div>
            </div>

            <?php if( $counter % 2 != 0 ) { ?>
                <div class="item__website--desktop hide__mobile">
                    <figure>
                        <img src="<?php the_field('afbeelding'); ?>" srcset="<?php the_field('afbeelding2x'); ?> 2x" alt="Schermafbeelding van <?php the_field("titel"); ?>">
                    </figure>
                </div>
            <?php } ?>
        </div>
    </article>
    <?php
    $counter++;
    endwhile;
    wp_reset_query();
    ?>
</section>

<section class="testimonial">
    <div class="container">
        <div class="container-small">
            <p class="testimonial__content">
                <?php the_field("testimonial_content"); ?>
            </p>
            <p class="testimonial__author">
                <?php the_field("testimonial_auteur"); ?> <?php if( get_field("testimonial_organisatie") ) { ?><span class="testimonial__seperator"></span><?php } ?> <?php the_field("testimonial_organisatie"); ?>
            </p>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="container-small">
            <h1><?php the_field("contact_titel"); ?></h1>

            <?php the_field("contact_content"); ?>
        </div>
    </div>
</section>

<section class="resources" id="resources">
    <div class="container">
        <div class="container-small">
            <h1><?php the_field("resources_titel"); ?></h1>
        </div>
        <div class="resources__items">
            <?php
            $args = array( 'post_type' => 'bookmarks', 'posts_per_page' => 9 );
            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ) : $loop->the_post();

            $bookmark_url = get_post_meta( get_the_ID(), '_WP_Plugin_Bookmarks-bookmarks-url', true );
            ?>
            <aside class="resources__item">
                <h3>
                    <a href="<?php echo $bookmark_url; ?>" title="Lees: <?php the_title(); ?>" target="_blank"><?php the_title(); ?></a>
                </h3>
                <p>
                    <a href="<?php echo $bookmark_url; ?>" title="Lees: <?php the_title(); ?>" target="_blank"><?php echo str_replace("www.", "", parse_url($bookmark_url, PHP_URL_HOST)); ?></a>
                </p>
            </aside>
            <?php
            endwhile;

            wp_reset_query();
            ?>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <h3>Shaun, WordPress webdesign</h3>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
        <p>
            Deze website werd gemaakt als oefening binnen een leeromgeving <br />en heeft geen enkel commercieel doel.
        </p>
        <div class="footer__icons">
            <a href="<?php echo get_field('facebook_link', 58); ?>" class="icon__facebook">Facebook</a>
            <a href="<?php echo get_field('twitter_link', 58); ?>" class="icon__twitter">Twitter</a>
            <a href="<?php echo get_field('rss_link', 58); ?>" class="icon__rss">RSS</a>
        </div>
    </div>
</footer>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "https://www.shaunjanssens.be/",
        "contactPoint" : [{
            "@type" : "ContactPoint",
            "telephone" : "+32-498061485",
            "contactType" : "customer support",
            "availableLanguage" : "Dutch"
        }],
        "sameAs" : [
            "https://twitter.com/shaunjanssens"
        ]
    }
</script>

<?php wp_footer(); ?>

</body>
</html>
