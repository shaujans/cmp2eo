<?php
/**
 * Created by PhpStorm.
 * User: shaunjanssens
 * Date: 10/04/16
 * Time: 17:24
 */
?>

<?php get_header(); ?>

<section class="main">
    <div class="container">
        <?php if ( have_posts() ) :

        if ( is_home() && ! is_front_page() ) : ?>
            <header>
                <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
            </header>

        <?php
        endif;

        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', get_post_format() );

        endwhile;

        the_posts_navigation();

        else :

        get_template_part( 'template-parts/content', 'none' );

        endif; ?>
    </div>
</section>

<?php get_footer(); ?>
