<?php get_header(); ?>

<section class="single">
    <div class="container">
        <div class="container-small">
            <?php
            while ( have_posts() ) : the_post();
                the_content();
            endwhile;
            ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
