<?php
/**
 * Created by PhpStorm.
 * User: shaunjanssens
 * Date: 10/04/16
 * Time: 17:24
 */
?>

<section class="contact">
    <div class="container">
        <div class="container-small">
            <h1><?php the_field("contact_titel", 58); ?></h1>

            <?php the_field("contact_content", 58); ?>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <h3>Shaun, WordPress webdesign</h3>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
        <p>
            Deze website werd gemaakt als oefening binnen een leeromgeving <br />en heeft geen enkel commercieel doel.
        </p>
        <div class="footer__icons">
            <a href="<?php echo get_field('facebook_link', 58); ?>" class="icon__facebook">Facebook</a>
            <a href="<?php echo get_field('twitter_link', 58); ?>" class="icon__twitter">Twitter</a>
            <a href="<?php echo get_field('rss_link', 58); ?>" class="icon__rss">RSS</a>
        </div>
    </div>
</footer>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "https://www.shaunjanssens.be/",
        "contactPoint" : [{
            "@type" : "ContactPoint",
            "telephone" : "+32-498061485",
            "contactType" : "customer support",
            "availableLanguage" : "Dutch"
        }],
        "sameAs" : [
            "https://twitter.com/shaunjanssens"
        ]
    }
</script>

<?php wp_footer(); ?>

</body>
</html>
