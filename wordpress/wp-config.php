<?php
define( 'WP_DEBUG', false );

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L>Ne[Cmgs6V*^m^]ooty`3K,b&pIn-M+4|~RgfdRQTEACw6+((]k%]|nIsqm++K4');
define('SECURE_AUTH_KEY',  ':`*iW*s!~w65.w7Yuvz.w_J&*bAp&{e{XmEP-L#T|+7@29=s(%xA?]TvTel!nS^D');
define('LOGGED_IN_KEY',    '/{IwsV)Qg^A<jEx7OeFyu:P WqXFaeR5vzVGk!n^&T38#b>B`XM_+OTJ+ujf?:G0');
define('NONCE_KEY',        '|@O._gfq+}la5o-hLjnJU-L^t;6Q,i@w,aMOE37u;/ni(swmtjiqn_ >cItJ?*4 ');
define('AUTH_SALT',        'D~PXH}`2+-[~Eqp_//rtr.ee1A Sm|{B+#wRD70JxdeAJ<<#Sz~%oL,]#ZQGY|GG');
define('SECURE_AUTH_SALT', 'F?K=4_t>ep` U53u<M&-P8PTnH=|!|tK6ysvp7nOi(;0A^x5h><>Y1~<|*cL#~dX');
define('LOGGED_IN_SALT',   'Uf&7YYo]Qqw#3wq.SjMs;}kbdM4?:0k#k3t;Ax$9BQ<CcUaKy37FFV1fphTM_Eco');
define('NONCE_SALT',       '7MRhVrfK#mY|&Q6k=,z++zBT>o?An[HjC@gy,tT$W$^eP^&sH>,&Abc&*aO~Aju<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
